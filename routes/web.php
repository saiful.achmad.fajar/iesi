<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\notifikasiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/notifikasi',[notifikasiController::class,'index']);
Route::get('/notifikasi/tambah',[notifikasiController::class,'tambah']);
Route::post('/notifikasi/store',[notifikasiController::class,'store']);
Route::get('/notifikasi/edit/{id}',[notifikasiController::class,'edit']);
Route::post('/notifikasi/update',[notifikasiController::class,'update']);
Route::get('/notifikasi/hapus/{id}',[notifikasiController::class,'hapus']);


