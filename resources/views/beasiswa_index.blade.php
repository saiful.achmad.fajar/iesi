@extends('layout.index')
@section('content')
<br></br>
<button type="button">
    <a href="{{url('beasiswa/tambah')}}">TAMBAH</a>
</button>
<table class="table-bordered table">
    <thead>    
        <tr>
            <th>Nama</th>
            <th>Pendidikan</th>
            <th>Tanggal Buka</th>
            <th>Tanggal Tutup</th>
            <th>Website</th>
            <th>Deskripsi</th>
            <th></th>
        </tr>
    </thead>
    @foreach($data as $key=>$value)
        <tr>
            <td>{{ $value->nama}}</td>
            <td>{{ $value->pendidikan}}</td>
            <td>{{ $value->tanggal_buka}}</td>
            <td>{{ $value->tanggal_tutup}}</td>
            <td>{{ $value->website}}</td>
            <td>{{ $value->deskripsi}}</td>

            <td>
                <form>
                <button type="button">
                    <a href="{{url('beasiswa/'.$value->id.'/edit')}}">EDIT</a>
                </button>
                </form>
            </td>

            <td>
                <form action="{{ url('beasiswa/hapus/'.$value->id) }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit">DELETE</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>
@endsection