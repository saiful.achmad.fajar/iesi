@extends('layout.index')
@section('content')
<br/>
<br>
EDIT DATA
<br/> 
<br/>
    <form method="POST" action="{{ url('beasiswa/'.$model->id) }}">
        @csrf
        <input type="hidden" name="_method" value="PUT">
        Nama : <input type="text" name="nama" value="{{ $model->nama}}"><br/>
        Pendidikan : <input type="text" name="pendidikan" value="{{ $model->pendidikan}}"><br/>
        Tanggal Buka : <input type="text" name="tanggal_buka" value="{{ $model->tanggal_buka}}"><br/>
        Tanggal Tutup : <input type="text" name="tanggal_tutup" value="{{ $model->tanggal_tutup}}"><br/>
        Website : <input type="text" name="website" value="{{ $model->website}}"><br/>
        Deskripsi : <input type="text" name="deskripsi" value="{{ $model->deskripsi}}"><br/>

        <button type="submit">SIMPAN</button>
    </form>
@endsection