<html>
<head>
	<title>Notifikasi</title>
</head>
<body>
  
	<a href="/notifikasi/tambah"> + Tambah Notifikasi Baru</a>
	
	<br/>
	<br/>
 
	<table border="1">
		<tr>
			<th>Judul</th>
			<th>Isi</th>
			<th>Waktu kirim</th>
		
		</tr>
		@foreach($notifikasi as $p)
		<tr>
			<td>{{ $p->judul }}</td>
			<td>{{ $p->isi }}</td>
			<td>{{ $p->waktu_kirim }}</td>
			
			<td>
				<a href="/notifikasi/edit/{{ $p->id }}">Edit</a>
				|
				<a href="/notifikasi/hapus/{{ $p->id }}">Hapus</a>
			</td>
		</tr>
		@endforeach
	</table>
 
 
</body>
</html>
