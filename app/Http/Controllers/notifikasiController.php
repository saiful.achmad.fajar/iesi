<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\m_notifikasi;

class notifikasiController extends Controller
{
    public function index(){
        $data=m_notifikasi::all();
        return view('notifikasi_index',['notifikasi'=>$data]);
    }
    public function tambah(){
        return view('notifikasi_tambah');
    }
    public function store(Request $request){
      m_notifikasi::insert(
            [
                'judul'=>$request->judul,
                'isi'=>$request->isi,
                'waktu_kirim'=>$request->waktu_kirim            ]
            );
            return redirect('/notifikasi');
    }

    public function edit($id){
        $data = m_notifikasi::where('id',$id)->get();

        return view('notifikasi_edit',['notifikasi'=>$data]);
    }

    public function update (Request $request){
        m_notifikasi::where('id',$request->id)->update(
            [
                'judul'=>$request->judul,
                'isi'=>$request->isi,
                'waktu_kirim'=>$request->waktu_kirim
            ]
        );
        return redirect('/notifikasi');
    }

    public function hapus($id){
        m_notifikasi::where('id',$id)->delete();
        return redirect('/notifikasi');
    }


}
