<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\beasiswa;


class SearchFilter extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    public function search(Request $request)
    {
        $nama_beasiswa = $request['nama_beasiswa'];
        $list = beasiswa::whereRaw("UPPER(nama_beasiswa) LIKE '%" . strtoupper($nama_beasiswa) . "%'")->get();
        return view('pencarian', ['list' => $list]);
    }
        // $id = Input::get('id');
        // $brand = Brand::firstOrNew(array('id' => $id));
        // $paginate = Misc::getSettings('admin-pagination');
        // $page_no = isset($_GET['page']) ? $_GET['page'] : 1;
        // $i = ($paginate * $page_no) - ($paginate - 1);
        // $appends = false;


        // if (beasiswa::has('category')) {
        //     $brandCat = BrandCategory::find(Input::get('category'));
        //     $newBrands = $brandCat->brands();
        //     $appends['category'] = Input::get('category');
        // } else {
        //     $newBrands = Brand::limit(-1);
        // }
    
    
        // $id_beasiswa = beasiswa::where('id_beasiswa',$request->id_beasiswa)->first();
        // $nama_beasiswa = beasiswa::where('nama_beasiswa',$request->nama_beasiswa)->first();
        // $pendidikan = beasiswa::where('pendidikan',$request->pendidikan)->first();

        // if(Input::has('tanggal_tutup')){
        //     $tanggal_tutup = beasiswa::where('tanggal_tutup',$request->tanggal_tutup)->first();
        //     return view::make
        // }
    //     return View::make('admin.brands.index')
    // ->with(array(
    //     'selected_cats' => $selected_cats,
    //     'brand' => $brand,
    //     'brands' => $brands,
    //     'brand_categories_list' => $brand_categories_list->lists('name', 'id'),
    //     'appends' => $appends,
    //     'i' => $i
    // ));



    //
    
}
