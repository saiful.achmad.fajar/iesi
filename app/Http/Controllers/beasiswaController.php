<?php

namespace App\Http\Controllers;

use App\Models\administrator;
use Illuminate\Http\Request;
use App\Models\beasiswa; 

class beasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = beasiswa::all();

        return view('beasiswa_index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new beasiswa;
        return view('beasiswa_create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new beasiswa();
        $model->nama = $request->nama;
        $model->pendidikan = $request->pendidikan;
        $model->tanggal_buka = $request->tanggal_buka;
        $model->tanggal_tutup = $request->tanggal_tutup;
        $model->website = $request->website;
        $model->deskripsi = $request->deskripsi;
        

        // $model->foto_profile = $request->foto_profile;
        //kita akan membuat code untuk upload file
        // if($request->file('foto_profile')){
        //     $file = $request->file('foto_profile');
        //     $nama_file = time().str_replace(" ", "", $file->getClientOriginalName());
        //     $file->move('foto', $nama_file);
        //     $model->foto_profile = $nama_file;
        // }
        $model->save();

        return redirect('beasiswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = beasiswa::find($id);
        return view('beasiswa_edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = beasiswa::find($id);
        $model->nama = $request->nama;
        $model->pendidikan = $request->pendidikan;
        $model->tanggal_buka = $request->tanggal_buka;
        $model->tanggal_tutup = $request->tanggal_tutup;
        $model->website = $request->website;
        $model->deskripsi = $request->deskripsi;

        $model->save();

        return redirect('beasiswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = beasiswa::find($id);
        $model->delete();
        return redirect('beasiswa');
    }
}
