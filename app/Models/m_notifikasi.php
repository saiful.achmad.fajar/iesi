<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class m_notifikasi extends Model
{
    protected $table='notifikasi';
    protected $fillable= [
        'judul',
        'isi',
        'waktu_kirim',
    ];
}
